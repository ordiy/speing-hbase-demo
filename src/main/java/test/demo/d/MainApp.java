package test.demo.d;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.Date;

/**
 * @version:
 * @Date: 2017/3/9 19:24
 */
public class MainApp {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(BootStrap.class);
        HbaseQueryJDOImplNew bean = context.getBean(HbaseQueryJDOImplNew.class);

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("CP00101");
        bean.queryLocDataByTable(arrayList,new Date(System.currentTimeMillis()-100000),new Date());
    }
}
