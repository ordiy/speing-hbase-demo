package test.demo.d;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

/**
 * @Description: ${NOTE}
 * @Author: pingxin
 * @version:
 * @Date: 2017/3/9 19:24
 */
@ImportResource({"application-context_4_hbase.xml"})
@ComponentScan()
public class BootStrap {
}
